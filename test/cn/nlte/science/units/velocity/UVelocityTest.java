package cn.nlte.science.units.velocity;

import cn.nlte.science.units.ValueWithUnit;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yetao
 */
public class UVelocityTest {
    
    public UVelocityTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UVelocity_mPers());
        assertEquals(1, data.getValue(),1E-10);
        data = data.convertTo(new UVelocity_KmPers());
        assertEquals(0.001, data.getValue(),1E-10);
        data = data.convertTo(new UVelocity_KmPerh());
        assertEquals(3.6, data.getValue(),1E-10);
        data = data.convertTo(new UVelocity_c());
        assertEquals(2.99792459e-8, data.getValue(),1E-14);
        data = data.convertTo(new UVelocity_Kn());
        assertEquals(1.94384449, data.getValue(),1E-7);
        data = data.convertTo(new UVelocity_miPerh());
        assertEquals(2.23693629, data.getValue(),1E-7);
        data = data.convertTo(new UVelocity_ftPers());
        assertEquals(3.2808399, data.getValue(),1E-6);
        data = data.convertTo(new UVelocity_inPers());
        assertEquals(39.3700787, data.getValue(),1E-6);
    }

    @Test
    public void testAdd() {
        
    }

    @Test
    public void testSub() {
        
    }

    @Test
    public void testMultiply() {
        
    }

    @Test
    public void testDivide() {
        
    }
    
}
