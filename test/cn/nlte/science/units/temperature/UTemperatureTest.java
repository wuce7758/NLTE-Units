package cn.nlte.science.units.temperature;

import cn.nlte.science.units.ValueWithUnit;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author yetao
 */
public class UTemperatureTest {
    
    public UTemperatureTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UTemperature_C());
        assertEquals(1, data.getValue(),1E-10);
        data = data.convertTo(new UTemperature_F());
        assertEquals(33.8, data.getValue(),1E-10);
        data = data.convertTo(new UTemperature_K());
        assertEquals(274.15, data.getValue(),1E-10);
        data = data.convertTo(new UTemperature_Ra());
        assertEquals(493.47, data.getValue(),1E-10);
        data = data.convertTo(new UTemperature_Re());
        assertEquals(0.8, data.getValue(),1E-10);
    }

    @Test
    public void testAdd() {
        
    }

    @Test
    public void testSub() {
        
    }

    @Test
    public void testMultiply() {
        
    }

    @Test
    public void testDivide() {
        
    }
    
    
}
