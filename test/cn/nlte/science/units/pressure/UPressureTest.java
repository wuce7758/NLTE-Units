/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cn.nlte.science.units.pressure;

import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.area.UArea_mm2;
import cn.nlte.science.units.force.UForce_N;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yetao
 */
public class UPressureTest {
    
    public UPressureTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UPressure_KPa());
        assertEquals(1, data.getValue(),1E-10);
        data = data.convertTo(new UPressure_Pa());
        assertEquals(1000, data.getValue(),1E-10);
        data = data.convertTo(new UPressure_MPa());
        assertEquals(0.001, data.getValue(),1E-10);
        data = data.convertTo(new UPressure_bar());
        assertEquals(0.01, data.getValue(),1E-10);
        data = data.convertTo(new UPressure_HPa());
        assertEquals(10, data.getValue(),1E-7);
        data = data.convertTo(new UPressure_mbar());
        assertEquals(10, data.getValue(),1E-7);
        data = data.convertTo(new UPressure_atm());
        assertEquals(0.00986923, data.getValue(),1E-6);
        data = data.convertTo(new UPressure_mmHg());
        assertEquals(7.50061683, data.getValue(),1E-6);
        data = data.convertTo(new UPressure_lbfPerft2());
        assertEquals(20.88543512, data.getValue(),1E-6);
        data = data.convertTo(new UPressure_lbfPerin2());
        assertEquals(0.14503774, data.getValue(),1E-6);
        data = data.convertTo(new UPressure_KgfPerm2());
        assertEquals(101.9716213, data.getValue(),1E-6);
    }

    @Test
    public void testAdd() {
        
    }

    @Test
    public void testSub() {
        
    }

    @Test
    public void testMultiply() {
        
    }

    @Test
    public void testDivide() {
        ValueWithUnit force = new ValueWithUnit(1.2, new UForce_N());
        ValueWithUnit area = new ValueWithUnit(1.0, new UArea_mm2());
        ValueWithUnit pressure = force.divide(area).convertTo(new UPressure_MPa());
        assertEquals(1.2, pressure.getValue(), 1E-6);
    }
    
}
