package cn.nlte.science.units;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author yetao
 */
public class UnitUtilsTest {

    public UnitUtilsTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, UnitUtils.getUnit("UTemperature_C"));
        assertEquals(1, data.getValue(), 1E-10);
        data = data.convertTo(UnitUtils.getUnit("UTemperature_F"));
        assertEquals(33.8, data.getValue(), 1E-10);
        data = data.convertTo(UnitUtils.getUnit("UTemperature_K"));
        assertEquals(274.15, data.getValue(), 1E-10);
        data = data.convertTo(UnitUtils.getUnit("UTemperature_Ra"));
        assertEquals(493.47, data.getValue(), 1E-10);
        data = data.convertTo(UnitUtils.getUnit("UTemperature_Re"));
        assertEquals(0.8, data.getValue(), 1E-10);
        DerivedUnit unit1 = UnitUtils.getUnit("UTemperature_C");
        DerivedUnit unit2 = UnitUtils.getUnit("UTemperature_C");
        assertEquals(true, unit1==unit2);
    }

}
