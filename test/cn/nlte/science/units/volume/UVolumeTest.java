package cn.nlte.science.units.volume;

import cn.nlte.science.units.ValueWithUnit;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yetao
 */
public class UVolumeTest {

    public UVolumeTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UVolume_m3());
        assertEquals(1, data.getValue(), 1E-10);
        data = data.convertTo(new UVolume_dal());
        assertEquals(100, data.getValue(), 1E-10);
        data = data.convertTo(new UVolume_dl());
        assertEquals(10000, data.getValue(), 1E-10);
        data = data.convertTo(new UVolume_ml());
        assertEquals(1000000, data.getValue(), 1E-10);
        data = data.convertTo(new UVolume_ft3());
        assertEquals(35.31466672, data.getValue(), 1E-6);
        data = data.convertTo(new UVolume_hl());
        assertEquals(10, data.getValue(), 1E-10);
        data = data.convertTo(new UVolume_l());
        assertEquals(1000, data.getValue(), 1E-10);
        data = data.convertTo(new UVolume_cl());
        assertEquals(100000, data.getValue(), 1E-10);
        data = data.convertTo(new UVolume_mm3());
        assertEquals(1000000000, data.getValue(), 1.0E-6);
        data = data.convertTo(new UVolume_yd3());
        assertEquals(1.30795062, data.getValue(), 1E-6);
        data = data.convertTo(new UVolume_in3());
        assertEquals(61023.74409473, data.getValue(), 1E-6);
    }

    @Test
    public void testAdd() {

    }

    @Test
    public void testSub() {

    }

    @Test
    public void testMultiply() {

    }

    @Test
    public void testDivide() {

    }

}
