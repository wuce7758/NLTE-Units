/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.nlte.science.units.energy;

import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.force.UForce_N;
import cn.nlte.science.units.length.ULength_mm;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author yetao
 */
public class UEnergyTest {

    public UEnergyTest() {
    }

    @Test
    public void testConvert() {
        // 测试转化是否正确
        ValueWithUnit data = new ValueWithUnit(1, new UEnergy_KJ());
        assertEquals(1, data.getValue(), 1E-10);
        data = data.convertTo(new UEnergy_J());
        assertEquals(1000, data.getValue(), 1E-10);
        data = data.convertTo(new UEnergy_MJ());
        assertEquals(0.001, data.getValue(), 1E-10);
        data = data.convertTo(new UEnergy_erg());
        assertEquals(1e+10, data.getValue(), 1E-14);
        data = data.convertTo(new UEnergy_Kcal());
        assertEquals(0.2389, data.getValue(), 1.0E-6);
        data = data.convertTo(new UEnergy_cal());
        assertEquals(238.9, data.getValue(), 1E-6);
        data = data.convertTo(new UEnergy_Btn());
        assertEquals(0.94781712, data.getValue(), 1E-6);
        data = data.convertTo(new UEnergy_KWh());
        assertEquals(0.00027778, data.getValue(), 1E-6);
        data = data.convertTo(new UEnergy_Kgfm());
        assertEquals(102, data.getValue(), 1E-1);
        data = data.convertTo(new UEnergy_ftlbf());
        assertEquals(737.6, data.getValue(), 1E-1);
    }

    @Test
    public void testAdd() {

    }

    @Test
    public void testSub() {

    }

    @Test
    public void testMultiply() {
        ValueWithUnit force = new ValueWithUnit(1.2, new UForce_N());
        ValueWithUnit length = new ValueWithUnit(1.2, new ULength_mm());
        ValueWithUnit energy = force.multiply(length);
        assertEquals(1.44E-3, energy.getValue(), 1E-6);
    }

    @Test
    public void testDivide() {

    }

}
