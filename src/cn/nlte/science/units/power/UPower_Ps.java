package cn.nlte.science.units.power;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.force.UForce_Kgf;
import cn.nlte.science.units.length.ULength_m;
import cn.nlte.science.units.time.UTime_s;
import java.util.ArrayList;
import java.util.List;

/**
 * 功率单位：Ps，公制马力
 *
 * @author yetao
 */
public class UPower_Ps extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "Ps";

    static {
        ValueWithUnit vwu = new ValueWithUnit(1.0, new ULength_m());
        factor = vwu.multiply(new ValueWithUnit(1.0, new UForce_Kgf())).divide(new ValueWithUnit(1.0, new UTime_s())).getBasicValue() * 75.0;
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
        elementList.add(new UnitElement(BasicUnit.s, -3.0));
    }

    public UPower_Ps() {
        super(description, elementList, factor, additional);
    }

}
