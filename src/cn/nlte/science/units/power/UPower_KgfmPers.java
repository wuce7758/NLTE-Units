package cn.nlte.science.units.power;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.energy.UEnergy_Kgfm;
import cn.nlte.science.units.time.UTime_s;
import java.util.ArrayList;
import java.util.List;

/**
 * ���ʵ�λ��Kgf*m/s
 *
 * @author yetao
 */
public class UPower_KgfmPers extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "Kgf*m/s";

    static {
        ValueWithUnit vwu = new ValueWithUnit(1.0, new UEnergy_Kgfm());
        factor = vwu.divide(new ValueWithUnit(1.0, new UTime_s())).getBasicValue();
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
        elementList.add(new UnitElement(BasicUnit.s, -3.0));
    }

    public UPower_KgfmPers() {
        super(description, elementList, factor, additional);
    }

}
