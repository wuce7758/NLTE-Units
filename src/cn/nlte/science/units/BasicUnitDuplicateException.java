package cn.nlte.science.units;

/**
 * 导出单位中有重复的基本单位异常
 *
 * @author yetao
 */
public class BasicUnitDuplicateException extends RuntimeException {

    public BasicUnitDuplicateException(String info) {
        super(info);
    }
}
