package cn.nlte.science.units.volume;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 体积单位：dal，十升
 *
 * @author yetao
 */
public class UVolume_dal extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 0.01;
    private final static double additional = 0.0;
    private final static String description = "dal";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 3.0));
    }

    public UVolume_dal() {
        super(description, elementList, factor, additional);
    }

}
