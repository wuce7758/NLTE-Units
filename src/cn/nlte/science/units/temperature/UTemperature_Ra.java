package cn.nlte.science.units.temperature;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 温度单位：Ra，兰氏度，1兰氏度=5/9K
 *
 * @author yetao
 */
public class UTemperature_Ra extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 5.0 / 9.0;
    private final static double additional = 0.0;
    private final static String description = "Ra";

    static {
        elementList.add(new UnitElement(BasicUnit.K, 1.0));
    }

    public UTemperature_Ra() {
        super(description, elementList, factor, additional);
    }

}
