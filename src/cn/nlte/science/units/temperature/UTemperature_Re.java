package cn.nlte.science.units.temperature;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 温度单位：Re，列氏度，1列氏度=1.25摄氏度
 *
 * @author yetao
 */
public class UTemperature_Re extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional;
    private final static String description = "Re";

    static {
        UTemperature_C tempC = new UTemperature_C();
        factor = tempC.getFactor() * 1.25;
        additional = tempC.getAddition();
        elementList.add(new UnitElement(BasicUnit.K, 1.0));
    }

    public UTemperature_Re() {
        super(description, elementList, factor, additional);
    }

}
