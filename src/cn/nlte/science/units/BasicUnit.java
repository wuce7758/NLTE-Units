package cn.nlte.science.units;

/**
 * 国际单位制的枚举
 *
 * @author yetao
 */
public enum BasicUnit {

    m, Kg, s, A, K, mol, cd
}
