package cn.nlte.science.units;

import java.util.HashMap;

/**
 * 本类封装单位管理的常用操作
 *
 * @author yetao
 */
public class UnitUtils {

    private static final HashMap<String, DerivedUnit> unitMap = new HashMap<>();

    /**
     * 根据单位的名称，返回单位的实例，用本方法获取的单位实例均是单例的（推荐使用）
     * 
     * 如果传入的类名不正确，则返回null
     *
     * @param name 单位的名称，即单位的类名，如：ULength_m
     * @return
     */
    public static DerivedUnit getUnit(String name) {
        // 查看unitMap是否有此对象
        DerivedUnit unit = unitMap.get(name);
        if (unit == null) {
            try {
                unit = (DerivedUnit) Class.forName("cn.nlte.science.units." + getPackageName(name) + "." + name).newInstance();
                unitMap.put(name, unit);
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
                return null;
            }
        }
        return unit;
    }

    /**
     * 解析包名
     *
     * @param className
     * @return
     */
    private static String getPackageName(String className) {
        String front = className.split("_")[0];
        try {
            return front.substring(1).toLowerCase();
        } catch (Exception ex) {
            return "";
        }
    }
}
