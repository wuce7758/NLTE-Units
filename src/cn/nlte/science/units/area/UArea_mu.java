package cn.nlte.science.units.area;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 面积单位：mu，定义亩的单位（市亩） 1亩=666.67平方米
 *
 * @author yetao
 */
public class UArea_mu extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1000.0*2.0/3.0;
    private final static double additional = 0.0;
    private final static String description = "mu";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
    }

    public UArea_mu() {
        super(description, elementList, factor, additional);
    }

}
