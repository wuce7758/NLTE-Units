package cn.nlte.science.units.area;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.length.ULength_mm;
import java.util.ArrayList;
import java.util.List;

/**
 * �����λ��mm2
 *
 * @author yetao
 */
public class UArea_mm2 extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "mm2";

    static {
        factor = Math.pow(new ULength_mm().getFactor(), 2.0);
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
    }

    public UArea_mm2() {
        super(description, elementList, factor, additional);
    }

}
