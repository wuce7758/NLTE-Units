package cn.nlte.science.units.area;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 面积单位：ha，定义公顷的单位 1公顷=10000平方米
 *
 * @author yetao
 */
public class UArea_ha extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 10000.0;
    private final static double additional = 0.0;
    private final static String description = "ha";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
    }

    public UArea_ha() {
        super(description, elementList, factor, additional);
    }

}
