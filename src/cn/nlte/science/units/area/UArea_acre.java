package cn.nlte.science.units.area;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 面积单位：acre，英亩，1英亩=43560平方英尺
 *
 * @author yetao
 */
public class UArea_acre extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "acre";

    static {
        factor = new UArea_ft2().getFactor() * 43560.0;
        elementList.add(new UnitElement(BasicUnit.m, 2.0));
    }

    public UArea_acre() {
        super(description, elementList, factor, additional);
    }

}
