package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * ���ȵ�λ��mm
 *
 * @author yetao
 */
public class ULength_mm extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 0.001;
    private final static double additional = 0.0;
    private final static String description = "mm";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_mm() {
        super(description, elementList, factor, additional);
    }

}
