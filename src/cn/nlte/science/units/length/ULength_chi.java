package cn.nlte.science.units.length;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 长度单位：chi，中国单位，1尺=1/3米
 *
 * @author yetao
 */
public class ULength_chi extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1.0 / 3.0;
    private final static double additional = 0.0;
    private final static String description = "chi";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
    }

    public ULength_chi() {
        super(description, elementList, factor, additional);
    }

}
