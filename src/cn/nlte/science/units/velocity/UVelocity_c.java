package cn.nlte.science.units.velocity;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 速度单位：c，光速
 *
 * @author yetao
 */
public class UVelocity_c extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 33356409.408550202;
    private final static double additional = 0.0;
    private final static String description = "c";

    static {
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
        elementList.add(new UnitElement(BasicUnit.s, -1.0));
    }

    public UVelocity_c() {
        super(description, elementList, factor, additional);
    }

}
