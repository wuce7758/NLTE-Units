package cn.nlte.science.units.velocity;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.length.ULength_mi;
import cn.nlte.science.units.time.UTime_h;
import java.util.ArrayList;
import java.util.List;

/**
 * 速度单位：mi/h，1英里/小时
 *
 * @author yetao
 */
public class UVelocity_miPerh extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "mi/h";

    static {
        ValueWithUnit vwu = new ValueWithUnit(1.0, new ULength_mi());
        factor = vwu.divide(new ValueWithUnit(1.0, new UTime_h())).getBasicValue();
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
        elementList.add(new UnitElement(BasicUnit.s, -1.0));
    }

    public UVelocity_miPerh() {
        super(description, elementList, factor, additional);
    }

}
