package cn.nlte.science.units.velocity;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.length.ULength_Km;
import cn.nlte.science.units.time.UTime_h;
import java.util.ArrayList;
import java.util.List;

/**
 * �ٶȵ�λ��Km/h
 *
 * @author yetao
 */
public class UVelocity_KmPerh extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "Km/h";

    static {
        ValueWithUnit vwu = new ValueWithUnit(1.0, new ULength_Km());
        factor = vwu.divide(new ValueWithUnit(1.0, new UTime_h())).getBasicValue();
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
        elementList.add(new UnitElement(BasicUnit.s, -1.0));
    }

    public UVelocity_KmPerh() {
        super(description, elementList, factor, additional);
    }

}
