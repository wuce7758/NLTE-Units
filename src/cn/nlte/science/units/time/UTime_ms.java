package cn.nlte.science.units.time;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * ʱ�䵥λ��ms������
 *
 * @author yetao
 */
public class UTime_ms extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 1.0E-3;
    private final static double additional = 0.0;
    private final static String description = "ms";

    static {
        elementList.add(new UnitElement(BasicUnit.s, 1.0));
    }

    public UTime_ms() {
        super(description, elementList, factor, additional);
    }

}
