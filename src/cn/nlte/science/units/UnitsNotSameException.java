package cn.nlte.science.units;

/**
 * 相加/减时，单位制不相等的异常
 *
 * @author yetao
 */
public class UnitsNotSameException extends RuntimeException {

    public UnitsNotSameException(String info) {
        super(info);
    }
}
