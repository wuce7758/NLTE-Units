package cn.nlte.science.units.pressure;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.area.UArea_ft2;
import cn.nlte.science.units.force.UForce_lbf;
import java.util.ArrayList;
import java.util.List;

/**
 * ѹǿ��λ��lbf/ft^2
 *
 * @author yetao
 */
public class UPressure_lbfPerft2 extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "lbf/ft^2";

    static {
        ValueWithUnit vwu = new ValueWithUnit(1.0, new UForce_lbf());
        factor = vwu.divide(new ValueWithUnit(1.0, new UArea_ft2())).getBasicValue();
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, -1.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UPressure_lbfPerft2() {
        super(description, elementList, factor, additional);
    }

}
