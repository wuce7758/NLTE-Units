package cn.nlte.science.units.pressure;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * ѹǿ��λ��mbar������
 *
 * @author yetao
 */
public class UPressure_mbar extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor = 100.0;
    private final static double additional = 0.0;
    private final static String description = "mbar";

    static {
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, -1.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UPressure_mbar() {
        super(description, elementList, factor, additional);
    }

}
