package cn.nlte.science.units.mass;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import java.util.ArrayList;
import java.util.List;

/**
 * 质量单位：lt，长吨
 *
 * @author yetao
 */
public class UMass_lt extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "lt";

    static {
        factor = new UMass_lb().getFactor() * 2240.0;
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
    }

    public UMass_lt() {
        super(description, elementList, factor, additional);
    }

}
