package cn.nlte.science.units.force;

import cn.nlte.science.units.BasicUnit;
import cn.nlte.science.units.DerivedUnit;
import cn.nlte.science.units.UnitElement;
import cn.nlte.science.units.ValueWithUnit;
import cn.nlte.science.units.accelerate.UAccelerate_g;
import cn.nlte.science.units.mass.UMass_Kg;
import java.util.ArrayList;
import java.util.List;

/**
 * 力单位：Kgf，1Kgf=1Kg*g（重力加速度）
 *
 * @author yetao
 */
public class UForce_Kgf extends DerivedUnit {

    private final static List<UnitElement> elementList = new ArrayList<>();
    private final static double factor;
    private final static double additional = 0.0;
    private final static String description = "Kgf";

    static {
        ValueWithUnit vwu = new ValueWithUnit(1.0, new UMass_Kg());
        factor = vwu.multiply(new ValueWithUnit(1.0, new UAccelerate_g())).getBasicValue();
        elementList.add(new UnitElement(BasicUnit.Kg, 1.0));
        elementList.add(new UnitElement(BasicUnit.m, 1.0));
        elementList.add(new UnitElement(BasicUnit.s, -2.0));
    }

    public UForce_Kgf() {
        super(description, elementList, factor, additional);
    }

}
